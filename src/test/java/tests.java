import org.junit.*;
import org.openqa.selenium.WebDriver;
import pages.*;

import static java.util.concurrent.TimeUnit.SECONDS;

public class tests{

    private static WebDriver driver;

    final static String logo_1 = "(//*[@class='logo__text'])[1]";
    final static String logo_2 = "(//*[@class='logo__text'])[2]";

    final static String electronics = "//*[text()='�����������']";
    final static String phone = "//*[text()='���������']";
    final static String headphones = "//*[text()='�������� � Bluetooth-���������']";

    static String browser = "";
    static String datetime = "";

    @BeforeClass
    public static void setup() {
        //Driver dr = new Driver(driver);
        driver =  cDriver.getChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, SECONDS);
        driver.manage().window().maximize();
        browser = cDriver.browser();
        datetime = cDriver.date_time();
    }


    @Test
    public void task_2_match_name_phone() {
        String first_phone = "(//*[contains(text(),'�������� Samsung')])[1]";
        String price = "//*[contains(text(),'40 000')]";

        MainPage mainpage = new MainPage(driver);
        mainpage.open();

        YaMarket yamarket_page = new YaMarket(driver);
        mainpage.go_market();
        Assert.assertEquals("������", yamarket_page.find_logo(logo_1));
        Assert.assertEquals("������", yamarket_page.find_logo(logo_2));

        yamarket_page.go(electronics);
        yamarket_page.go(phone);
        PhonePage phpage = new PhonePage(driver);
        phpage.filters();

        String name_phone = yamarket_page.get_first_name(first_phone, price);
       // System.out.println(name_phone + '\n' + phpage.get_name_phone());
        Assert.assertEquals( name_phone, phpage.get_name_phone());
        System.out.println("Successful run test task_2_match_name_phone");
    }

    @Test
    public void task_2_match_name_headphones() {

        String first_headphones = "(//*[contains(text(),'�������� Beats')])[1]";
        String price = "//*[contains(text(),'17 ')]";

        MainPage mainpage = new MainPage(driver);
        mainpage.open();

        YaMarket yamarket_page = new YaMarket(driver);
        mainpage.go_market();
        Assert.assertEquals("������", yamarket_page.find_logo(logo_1));
        Assert.assertEquals("������", yamarket_page.find_logo(logo_2));

        yamarket_page.go(electronics);
        yamarket_page.go(headphones);
        Headphones hppage = new Headphones(driver);
        hppage.filters();

        String name_headphones = yamarket_page.get_first_name(first_headphones,price);
        // System.out.println(name_headphones + '\n' + hppage.get_name_phone());
        Assert.assertEquals( name_headphones, hppage.get_name_phone());
        System.out.println("Successful run test task_2_match_name_headphones");
    }

    @Test
    public void task_3() {

        MainPage mainpage = new MainPage(driver);

        mainpage.open();
        String search_sys = mainpage.logo();
        String tab = mainpage.search();

        AlfaPage alfa_page = new AlfaPage(driver);
        alfa_page.job();

        //System.out.println(alfa_page.text());

        boolean res = alfa_page.create(datetime + " " + browser + " " + search_sys);
        Assert.assertTrue(res);
        boolean res_write = alfa_page.write(datetime + " " + browser + " " + search_sys, alfa_page.text());
        Assert.assertTrue(res_write);

        mainpage.return_old_page(tab);
        System.out.println("Successful run test task_3 ");
    }


    @AfterClass
    public static void quit() {
        driver.manage().timeouts().implicitlyWait(5, SECONDS);
        driver.close();
    }
}