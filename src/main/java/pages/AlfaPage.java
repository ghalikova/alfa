package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AlfaPage {

    private WebDriver driver;
    String job = "[title='��������']";
    String about_us = "//h3[text()='� ���']";
    String sentence_1 = "(//*[@class='aligncenter font-white font-18 font-sm-16'])[1]";
    String sentence_2 = "(//*[@class='aligncenter font-white font-18 font-sm-16 top-12'])[1]";
    String sentence_3 = "(//*[@class='aligncenter font-white font-18 font-sm-16 top-12'])[2]";


    public AlfaPage(WebDriver driver) {
        this.driver = driver;
    }

    public String text() {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("var bxpath = document.evaluate (\"//h3[text()='� ���']\",\n" +
                "            document,\n" +
                "            null,\n" +
                "            XPathResult.FIRST_ORDERED_NODE_TYPE,\n" +
                "            null);\n" +
                "            bxpath.singleNodeValue.scrollIntoView();\n" +
                "undefined\n");

        return driver.findElement(By.xpath(sentence_1)).getText() + "\n" + driver.findElement(By.xpath(sentence_2)).getText() + "\n" +
                driver.findElement(By.xpath(sentence_3)).getText();
    }

    public void job() {
        driver.findElement(By.cssSelector(job)).click();
    }

    public boolean create(String name) {
        File file = new File( name + ".txt");
        boolean created;
        try
        {
            created = file.createNewFile();
            if(created)
                System.out.println("File has been created");
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
            created = false;
        }
        return created;
    }

    public boolean write(String name, String text) {
        boolean write_;
        try(FileWriter writer = new FileWriter(name + ".txt", false))
        {
            writer.write(text);
            writer.flush();
            write_ = true;
        }
            catch(IOException ex){
                System.out.println(ex.getMessage());
                write_ = false;
        }
        return write_;
    }
}
