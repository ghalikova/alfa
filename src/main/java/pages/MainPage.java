package pages;

import org.openqa.selenium.*;

import java.util.ArrayList;

import static java.util.concurrent.TimeUnit.SECONDS;

public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    String ya = "https://yandex.ru/ ";
    final static String link_market = "[data-id='market']";
    String logo = "[class='media-infinity-footer__part media-infinity-footer__part_yandex']";
    String search_input = "[id='text']";
    String search_result = "[accesskey='1']";


    public void open() {
        driver.get(ya);
    }

    //go to market page
    public void go_market() {
        driver.findElement(By.cssSelector(link_market)).click();
    }

    //get name system search
    public String logo() {
        //scroll to down page
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("var bxpath = document.evaluate (\"//*[@class='media-infinity-footer__part media-infinity-footer__part_yandex']\",\n" +
                "            document,\n" +
                "            null,\n" +
                "            XPathResult.FIRST_ORDERED_NODE_TYPE,\n" +
                "            null);\n" +
                "            bxpath.singleNodeValue.scrollIntoView();\n");


        driver.manage().timeouts().implicitlyWait(20,SECONDS);
        String tmp = "";
        tmp = driver.findElement(By.cssSelector(logo)).getText().toLowerCase();
        return tmp.substring(2);
    }

    //go to alfa page
    public String search() {
        driver.findElement(By.cssSelector(search_input)).sendKeys("����� ����" + Keys.ENTER);

        String oldTab = driver.getWindowHandle();
        driver.findElement(By.cssSelector(search_result)).click();
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        newTab.remove(oldTab);
        driver.switchTo().window(newTab.get(0));
        return oldTab;
    }

    //go to first page
    public void return_old_page(String tab) {
        driver.close();
        // change focus back to old tab
        driver.switchTo().window(tab);
    }
}
