package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;


public class PhonePage {

	private WebDriver driver;

	String name_ph = "[class='title title_size_28 title_bold_yes']";
	String samsung = "//*[text()='Samsung']";
	String price = "[id='glpricefrom']";


	public PhonePage(WebDriver driver) {
		this.driver = driver;
	}

	public void filters() {
		driver.findElement(By.xpath(samsung)).click();
		driver.findElement(By.cssSelector(price)).sendKeys("40000" + Keys.ENTER);

	}
	public String get_name_phone() {
		return driver.findElement(By.cssSelector(name_ph)).getText();
	}

}
