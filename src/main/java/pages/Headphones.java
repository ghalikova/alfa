package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;


public class Headphones {

    private WebDriver driver;

    String name_ph = "[class='title title_size_28 title_bold_yes']";
    String beats = "//*[text()='Beats']";
    String begin_price = "[id='glpricefrom']";
    String end_price = "[id='glpriceto']";
    public Headphones(WebDriver driver) {
        this.driver = driver;
    }

    public void filters() {

        driver.findElement(By.cssSelector(begin_price)).sendKeys("17000" + Keys.ENTER);
        driver.findElement(By.cssSelector(end_price)).sendKeys("25000" + Keys.ENTER);
        driver.findElement(By.xpath(beats)).click();
    }
    public String get_name_phone() {
        return driver.findElement(By.cssSelector(name_ph)).getText();
    }
}
