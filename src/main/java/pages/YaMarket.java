package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static java.util.concurrent.TimeUnit.SECONDS;

public class YaMarket {

    private WebDriver driver;

    public YaMarket(WebDriver driver) {
        this.driver = driver;
    }

    public String find_logo(String logo) {
        return driver.findElement(By.xpath(logo)).getText();
    }

    public void go(String str1) {
        driver.findElement(By.xpath(str1)).click();
    }

    public String get_first_name(String name, String str) {

       WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath(name))));
      //  driver.manage().timeouts().pageLoadTimeout(30, SECONDS);

        WebElement first_ph = driver.findElement(By.xpath(name));
        String name_phone = first_ph.getText();
        first_ph.click();
        return name_phone;
    }
}
