import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class cDriver {
    private static WebDriver driver;

    private cDriver() {}

    public static WebDriver getChromeDriver() {
        if (driver == null) {
            // ��������� ��������� ����������� � ��������� ���� �� ������������ ��������
            System.setProperty("WebDriver.chrome.driver", "/chromedriver");
            driver = new ChromeDriver();
        }
        return driver;
    }

    //get name browser
    public static String browser() {
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();
        return browserName;
    }

    //get date and time beginning launch of the test
    public static String date_time() {

        DateFormat dateFormat = new SimpleDateFormat("d MMM yyyy HH mm");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

}