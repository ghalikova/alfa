import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class task_1{

    public task_1() {}

    static int[] read(String filename) {


        int[] array = new int[52];
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(filename));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            array = Arrays.stream(line.split(","))
                .mapToInt(Integer::parseInt).toArray();

            //System.out.println(Arrays.toString(array));
        }
        return array;
    }

    public static void main(String[] args) {

        int[] intArr = read("date.txt");
        //int[] intArr = {5,9,1,17,10,2,6,12,7,15,20,4,18,0,14,3,8,11,16,19,13};

        Arrays.sort(intArr);
        System.out.println("Sort Ascending " + Arrays.toString(intArr));


        int[] arrDesc = Arrays.stream(intArr).boxed()
                .sorted(Collections.reverseOrder())
                .mapToInt(Integer::intValue)
                .toArray();

        System.out.println("Sort Descending " + Arrays.toString(arrDesc));
    }

}